<?php

namespace Creativehandles\ChGallery;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Creativehandles\ChGallery\Skeleton\SkeletonClass
 */
class ChGalleryFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'ch-gallery';
    }
}
