<?php

/**
 * Gallery plugin routes
 */
Route::group(['name' => 'gallery', 'groupName' => 'gallery'], function () {
    Route::get('/gallery', 'PluginsControllers\GalleryController@index')->name('gallery');
    Route::post('/uploadImage', 'PluginsControllers\GalleryController@uploadImage')->name('uploadImage');
    Route::post('/saveImage', 'PluginsControllers\GalleryController@saveImage')->name('saveImage');
    Route::post('/deleteImage', 'PluginsControllers\GalleryController@deleteImage')->name('deleteImage');
    Route::get('/gallery/search', 'PluginsControllers\GalleryController@searchImage')->name('ajaxSearchImage');
});
